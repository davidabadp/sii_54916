#include "DatosMemCompartida.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>


int main()
{
int fd;

DatosMemCompartida *pdat;
fd=open("/tmp/dat",O_RDWR);
if(fd<0)
{
   perror("Error al abrir el fichero");
}

pdat=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));

if(pdat==MAP_FAILED)
{
     perror("Error en la proyección del Fichero");
     close(fd);
}
close(fd);

while(pdat!=NULL)
{
  double y = ((pdat->raqueta1.y1 + pdat->raqueta1.y2) / 2.0);
  if(pdat->esfera.centro.y<y) {pdat->accion=-1;}
  else if(pdat->esfera.centro.y==y) {pdat->accion=0;}
  else if(pdat->esfera.centro.y>y) {pdat->accion=1;}
  usleep(25000);
}
unlink("/tmp/dat");
return 1;
}
