// MundoCliente.cpp: implementation of the CMundoCliente class.
//Propiedad del alumno 54916
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "Puntuaciones.h"
#include "glut.h"
#include <iostream>
#include <string>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()//Constructor de mundo
{
	Init();
}

CMundoCliente::~CMundoCliente()//Destructor de mundo
{
       munmap(pdat,sizeof(dat));
       unlink("/tmp/dat");
       
       /*close(fifo_servidor_cliente);
       unlink("/tmp/FIFO_SERVIDOR_CLIENTE");
       
       close(fifo_cliente_servidor);
       unlink("/tmp/FIFO_CLIENTE_SERVIDOR");*/
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	  
        char cad[100];
        s_comunicacion.Receive(cad, sizeof(cad));
        //read(fifo_servidor_cliente,cad,sizeof(cad));
        
        sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,
        &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
        &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,
        &puntos1, &puntos2);

        pdat->esfera=esfera;
        pdat->raqueta1=jugador1;

        if (pdat->accion==-1){OnKeyboardDown('s',0,0);}
        else if (pdat->accion==0){}
        else if(pdat->accion==1){OnKeyboardDown('w',0,0);}
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
        char tecla[]="0";
	switch(key)
	{
	/*case 'a':jugador1.velocidad.x=-1;break;
	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;*/
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;

	}
	//write(fifo_cliente_servidor,&key,sizeof(key));
	s_comunicacion.Send(tecla,sizeof(tecla));
}

void CMundoCliente::Init()
{
//CONEXION CON SERVIDOR
	char ip[]="127.0.0.1";
	char nombre[50];
	printf("Introduzca su nombre:\n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);

//ENVIO DEL NOMBRE AL SERVIDOR

	s_comunicacion.Send(nombre,sizeof(nombre));
	
	
        int fdd;
        char *pfdd;

        fdd=open("/tmp/dat",O_CREAT|O_TRUNC|O_RDWR,0666);
        if(fdd<0)
        {
            perror("Error en creacion de Datos Compartidos");
        }
        dat.esfera=esfera;
        dat.raqueta1=jugador1;
        dat.accion=0;
        write(fdd,&dat,sizeof(dat));

        pdat=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(dat),PROT_READ|PROT_WRITE,MAP_SHARED,fdd,0));

        close(fdd);
        if(pdat==MAP_FAILED)
        {
            perror("Error en la proyección de Datos Compartidos");
        }
        
        /*//CREACION Y APERTURA EN MODO LECTURA FIFO SERVIDOR CLIENTE
	mkfifo("/tmp/FIFO_SERVIDOR_CLIENTE",0600);

	fifo_servidor_cliente=open("/tmp/FIFO_SERVIDOR_CLIENTE",O_RDONLY);
	if(fifo_servidor_cliente<0)
	{
		perror("Error en la creacion de FIFO_SERVIDOR_CLIENTE");
	}
	
       //CREACION Y APERTURA EN MODO LECTURA FIFO CLIENTE SERVIDOR
	mkfifo("/tmp/FIFO_CLIENTE_SERVIDOR",0600);

	fifo_cliente_servidor=open("/tmp/FIFO_CLIENTE_SERVIDOR",O_WRONLY);
	if(fifo_cliente_servidor<0)
	{
		perror("Error en la creacion de FIFO_CLIENTE_SERVIDOR");
	}*/	

       Plano p;
       //pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

       //superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
