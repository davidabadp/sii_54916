// Esfera.cpp: implementation of the Esfera class.
//Propiedad del alumno 54916
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()//Constructor de esfera
{
	radio=0.5f;
	velocidad.x=4;
	velocidad.y=4;
}

Esfera::~Esfera()//Destructor de esfera
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro = centro + velocidad*t;
}


