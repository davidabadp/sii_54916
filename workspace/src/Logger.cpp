// Logger.cpp: implementation of the Logger class.
//Propiedad del alumno 54916
//////////////////////////////////////////////////////////////////////
#pragma once
#include <stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <Puntuaciones.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
int main()
{
int fd;
Puntuaciones puntos;
//Se crea el FIFO
if (mkfifo("/tmp/FIFO_LOGGER",0666)<0)
{
    perror("Error al crear el FIFO");
}

if ((fd=open("/tmp/FIFO_LOGGER",O_RDONLY))<0) 
{
    perror("Error al abrir el FIFO");
}

while(read(fd,&puntos,sizeof(puntos))!=NULL)
{
  if ((read(fd,&puntos,sizeof(puntos)<0) ))
  {
    perror("Error al leer el FIFO");
  }
  if (puntos.gol==1) 
  {
    printf("Jugador 1 marca 1 punto,lleva un total de %d puntos\n",puntos.jugador1);
  }

  else if (puntos.gol==2) 
  {
    printf("Jugador 2 marca 1 punto,lleva un total de %d puntos\n",puntos.jugador2);
  }
}
  close(fd);
  unlink("/tmp/FIFO_LOGGER");
  return 0;
} 
